/*
 * @file: project.js
 * @description: It Contain function layer for project controller.
 * @author: Jasdeep Singh
 */

import { successAction, failAction } from "../utilities/response";
import { addProject, projectList, updateProject } from "../services/project";
import Message from "../utilities/messages";

/**************** Add project ***********/
export const add = async (req, res, next) => {
  const payload = req.body;
  if (req.user.role !== ROLE.ADMIN) {
    return res.status(400).json(failAction(Message.unauthorizedUser));
  }
  try {
    const data = await addProject(payload);
    res.status(200).json(successAction(data, Message.registerSuccess));
  } catch (error) {
    res.status(400).json(failAction(error.message));
  }
};

/**************** list of projects ***********/
export const list = async (req, res, next) => {
  const payload = req.query;
  try {
    const data = await projectList(payload);
    res.status(200).json(successAction(data, Message.success));
  } catch (error) {
    res.status(400).json(failAction(error.message));
  }
};
/**************** update projects ***********/
export const update = async (req, res, next) => {
  const payload = req.body;
  if (req.user.role !== ROLE.ADMIN) {
    return res.status(400).json(failAction(Message.unauthorizedUser));
  }
  try {
    const data = await updateProject(payload);
    res.status(200).json(successAction(data, Message.success));
  } catch (error) {
    res.status(400).json(failAction(error.message));
  }
};
