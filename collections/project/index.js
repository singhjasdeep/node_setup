/*
 * @file: index.js
 * @description: It Contain function layer for user collection.
 * @author: Jasdeep Singh
 */

import mongoose from "mongoose";
import dbSchema from "./db-schema";

class ProjectClass {
  static add(payload) {
    return this(payload).save();
  }
  static findOneByCondition(condition) {
    return this.findOne(condition);
  }
  static findByCondition(condition) {
    return this.find({ status: { $ne: 4 }, ...condition });
  }
  static updateProject(payload) {
    let updateData = {
      $set: {
        ...payload
      }
    };
    return this.findByIdAndUpdate(payload.id, updateData, { new: true });
  }
}

dbSchema.loadClass(ProjectClass);

export default mongoose.model("projects", dbSchema);
