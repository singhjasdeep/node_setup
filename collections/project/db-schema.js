/*
 * @file: db-schema.js
 * @description: It Contain db schema for user collection.
 * @author: Jasdeep Singh
 */

import mongoose from "mongoose";
import { ROLE } from "../../utilities/constants";

const projectSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      default: ""
    },
    teamMembers: [
      {
        _id: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        role: { type: Number, default: ROLE.ASSOCIATE },
        status: { type: Boolean, default: true },
        createdAt: {
          type: Date,
          default: new Date()
        }
      }
    ],
    status: {
      type: Number,
      default: 0 // 0 => pending, 1 => start, 2 => hold, 3=> close, 4 => delete
    },
    startDate: {
      type: Date,
      default: null
    },
    endDate: {
      type: Date,
      default: null
    },
    closeDate: {
      type: Date,
      default: null
    },
    totalHours: {
      type: Number,
      default: 0
    },
    paymentMode: {
      type: String,
      default: "" // fixed or hourly
    }
  },
  { timestamps: true }
);

export default projectSchema;
