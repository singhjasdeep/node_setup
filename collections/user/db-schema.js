/*
 * @file: db-schema.js
 * @description: It Contain db schema for user collection.
 * @author: Jasdeep Singh
 */

import mongoose from "mongoose";

const userSchema = new mongoose.Schema(
  {
    uid: {
      type: String,
      required: null
    },
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      default: ""
    },
    email: {
      type: String,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    loginToken: [
      {
        token: {
          type: String,
          required: true
        },
        createdAt: {
          type: Date,
          default: new Date()
        }
      }
    ],
    status: {
      type: Number,
      default: 1 // 0 account deleted, 1 active, 2 block
    },
    lastLogin: {
      type: Date,
      default: null
    },
    role: {
      type: Number,
      default: 4 // 1=>VP, 2=>ADMIN, 3=>MANAGER, 4=>ASSOCIATE, HR=>HR
    },
    joinDate: {
      type: Date,
      default: null
    },
    appraisalDate: {
      type: Date,
      default: null
    },
    department: {
      type: String, // OS, BD, BDG, MOBILITY, MOD
      default: null
    }
  },
  { timestamps: true }
);

export default userSchema;
