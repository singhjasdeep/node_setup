/*
 * @file: index.js
 * @description: It's combine all routers.
 * @author: Jasdeep Singh
 */

import user from "./user";
import project from "./project";

/*********** Combine all Routes ********************/
export default [...user, ...project];
