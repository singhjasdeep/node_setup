/*
 * @file: update-status.js
 * @description: It Contain update user status router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { status } from "../../controllers/user";
import { checkToken } from "../../utilities/universal";

const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/user/status:
 *  put:
 *   tags: ["user"]
 *   summary: user status update api
 *   description: api used to update users status
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: user
 *        description: update user status by admin.
 *        schema:
 *         type: object
 *         required:
 *          - update user status
 *         properties:
 *           userId:
 *             type: string
 *             required: true
 *           status:
 *             type: integer
 *             required: true
 *   responses:
 *    '200':
 *    description: success
 */
const userSchema = Joi.object({
  userId: Joi.string()
    .trim()
    .required()
    .label("User Id"),
  status: Joi.number()
    .required()
    .label("Status")
});

app.put(
  "/user/status",
  validator.body(userSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  status
);

export default app;
