/*
 * @file: update.js
 * @description: It Contain update router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { updateUser } from "../../controllers/user";
import { ROLE, DEPARTMENT } from "../../utilities/constants";
import { checkToken } from "../../utilities/universal";

const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/user:
 *  put:
 *   tags: ["user"]
 *   summary: user info update api
 *   description: api used to update users info
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: user
 *        description: update users info.
 *        schema:
 *         type: object
 *         required:
 *          - user add
 *         properties:
 *           userId:
 *             type: string
 *             required:
 *           firstName:
 *             type: string
 *             required:
 *           lastName:
 *             type: string
 *           role:
 *             type: integer
 *             required:
 *           joinDate:
 *             type: string
 *             format: date-time
 *             required:
 *           appraisalDate:
 *             type: string
 *             format: date-time
 *             required:
 *           department:
 *             type: string
 *             required:
 *   responses:
 *    '200':
 *    description: success
 */

const userSchema = Joi.object({
  userId: Joi.string()
    .required()
    .label("User Id"),
  firstName: Joi.string()
    .required()
    .label("First name"),
  lastName: Joi.string()
    .optional()
    .allow("")
    .label("Last name"),
  role: Joi.number()
    .valid(ROLE.VP, ROLE.ADMIN, ROLE.MANAGER, ROLE.ASSOCIATE, ROLE.HR)
    .required()
    .label("Role"),
  joinDate: Joi.any()
    .required()
    .label("Joing Date"),
  appraisalDate: Joi.any()
    .required()
    .label("Appraisal Date"),
  department: Joi.number()
    .valid(
      DEPARTMENT.OS,
      DEPARTMENT.BD,
      DEPARTMENT.BDG,
      DEPARTMENT.MOBILITY,
      DEPARTMENT.MOD
    )
    .required()
    .label("Department")
});

app.post(
  "/user",
  validator.body(userSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  updateUser
);

export default app;
