/*
 * @file: add.js
 * @description: It Contain add router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { addUser } from "../../controllers/user";
import { ROLE, DEPARTMENT } from "../../utilities/constants";
import { checkToken } from "../../utilities/universal";

const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/user:
 *  post:
 *   tags: ["user"]
 *   summary: user add api
 *   description: api used to add users
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: user
 *        description: The user to create.
 *        schema:
 *         type: object
 *         required:
 *          - user add
 *         properties:
 *           firstName:
 *             type: string
 *             required:
 *           lastName:
 *             type: string
 *           email:
 *             type: string
 *             required:
 *           password:
 *             type: string
 *             required:
 *           role:
 *             type: integer
 *             required:
 *           joinDate:
 *             type: string
 *             format: date-time
 *             required:
 *           appraisalDate:
 *             type: string
 *             format: date-time
 *             required:
 *           department:
 *             type: string
 *             required:
 *   responses:
 *    '200':
 *    description: success
 */

const userSchema = Joi.object({
  firstName: Joi.string()
    .required()
    .label("First name"),
  lastName: Joi.string()
    .optional()
    .allow("")
    .label("Last name"),
  email: Joi.string()
    .email()
    .required()
    .label("Email"),
  password: Joi.string()
    .trim()
    .required()
    .label("Password"),
  role: Joi.number()
    .valid(ROLE.VP, ROLE.ADMIN, ROLE.MANAGER, ROLE.ASSOCIATE, ROLE.HR)
    .required()
    .label("Role"),
  joinDate: Joi.any()
    .required()
    .label("Joing Date"),
  appraisalDate: Joi.any()
    .required()
    .label("Appraisal Date"),
  department: Joi.number()
    .valid(
      DEPARTMENT.OS,
      DEPARTMENT.BD,
      DEPARTMENT.BDG,
      DEPARTMENT.MOBILITY,
      DEPARTMENT.MOD
    )
    .required()
    .label("Department")
});

app.post(
  "/user",
  validator.body(userSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  addUser
);

export default app;
