/*
 * @file: index.js
 * @description: It's combine all user routers.
 * @author: Jasdeep Singh
 */

import add from "./add";
import login from "./login";
import logout from "./logout";
import list from "./list";
import updateUser from "./update";
import updateStatus from "./update-status";

export default [add, login, logout, list, updateStatus, updateUser];
