/*
 * @file: update.js
 * @description: It Contain update project router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { update } from "../../controllers/project";
import { checkToken } from "../../utilities/universal";

const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/project:
 *  put:
 *   tags: ["project"]
 *   summary: update project info and status api
 *   description: api used to update project info and status
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: project
 *        description: update project status and info, and status should be 1 => start, 2 => hold, 3=> close, 4=> delete
 *        schema:
 *         type: object
 *         required:
 *          - update project info
 *         properties:
 *           id:
 *             type: string
 *             required: true
 *           status:
 *             type: integer
 *           closeDate:
 *             type: string
 *             format: date-time
 *   responses:
 *    '200':
 *    description: success
 */
const projectSchema = Joi.object({
  id: Joi.string()
    .trim()
    .required()
    .label("Project Id"),
  status: Joi.number()
    .optional()
    .allow(null)
    .label("Status"),
  closeDate: Joi.any()
    .optional()
    .allow(null, "")
    .label("Close Date")
});

app.put(
  "/project",
  validator.body(projectSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  update
);

export default app;
