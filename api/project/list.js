/*
 * @file: list.js
 * @description: It Contain list router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { list } from "../../controllers/project";
import { checkToken } from "../../utilities/universal";

const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/project:
 *  get:
 *   tags: ["project"]
 *   summary: project list api
 *   description: api used to project list
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: query
 *        name: pageNumber
 *      - in: query
 *        name: search
 *   responses:
 *    '200':
 *    description: success
 */

const projectSchema = Joi.object({
  pageNumber: Joi.number()
    .optional()
    .allow("")
    .label("Page number"),
  search: Joi.string()
    .optional()
    .allow("")
    .label("Search text")
});

app.get(
  "/project",
  validator.query(projectSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  list
);

export default app;
