/*
 * @file: index.js
 * @description: It's combine all project routers.
 * @author: Jasdeep Singh
 */

import add from "./add";
import list from "./list";
import update from "./update";

export default [add, list, update];
