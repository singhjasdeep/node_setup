/*
 * @file: add.js
 * @description: It Contain project add router/api.
 * @author: Jasdeep Singh
 */
import express from "express";
import { createValidator } from "express-joi-validation";
import Joi from "@hapi/joi";
import { add } from "../../controllers/project";
import { checkToken } from "../../utilities/universal";
import { ROLE, PAYMENT_MODE } from "../../utilities/constants";
const app = express();
const validator = createValidator({ passError: true });
/**
 * @swagger
 * /api/v1/project:
 *  post:
 *   tags: ["project"]
 *   summary: add project api
 *   description: api used to add project
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: user
 *        description: The user to add project.
 *        schema:
 *         type: object
 *         required:
 *          - project add
 *         properties:
 *           name:
 *             type: string
 *             required:
 *           description:
 *             type: string
 *             optional:
 *           teamMembers:
 *             type: array
 *             items:
 *              type: object
 *              properties:
 *               _id:
 *                type: string
 *                required: true
 *               role:
 *                type: string
 *                required: true
 *             required:
 *           startDate:
 *             type: string
 *             format: date-time
 *             required:
 *           endDate:
 *             type: string
 *             format: date-time
 *             required:
 *           closeDate:
 *             type: string
 *             format: date-time
 *           totalHours:
 *             type: integer
 *             required:
 *           paymentMode:
 *             type: string
 *             required:
 *   responses:
 *    '200':
 *    description: success
 */
const projectSchema = Joi.object({
  name: Joi.string()
    .required()
    .label("Competition name"),
  description: Joi.string()
    .optional()
    .allow("")
    .label("Description"),
  startDate: Joi.any()
    .required()
    .label("Start Date"),
  endDate: Joi.any()
    .required()
    .label("End Date"),
  teamMembers: Joi.array()
    .items(
      Joi.object({
        _id: Joi.string()
          .trim()
          .required()
          .label("User Id"),
        role: Joi.number()
          .required()
          .valid(ROLE.MANAGER, ROLE.ASSOCIATE)
          .label("Role")
      })
    )
    .required()
    .label("Users"),
  closeDate: Joi.any()
    .optional()
    .allow("", null)
    .label("Close Date"),
  totalHours: Joi.number()
    .optional()
    .allow("", null)
    .label("Total Hours"),
  paymentMode: Joi.string()
    .required()
    .valid(PAYMENT_MODE.FIXED, PAYMENT_MODE.HOURLY)
    .label("Total Hours")
});

app.post(
  "/project",
  validator.body(projectSchema, {
    joi: { convert: true, allowUnknown: false }
  }),
  checkToken,
  add
);

export default app;
