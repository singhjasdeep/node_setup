/*
 * @file: swagger-config.js
 * @description: It Contain swagger configrations.
 * @author: Jasdeep Singh
 */
import swaggerJsDocs from "swagger-jsdoc";

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "360 performance management system project apis",
      version: "1.0",
      description: "All api end points",
      contact: {
        name: "Jasdeep Singh"
      },
      servers: ["http://localhost:3000"]
    },
    produces: ["application/json"]
  },
  apis: ["./api/user/*.js", "./api/project/*.js"]
};
export default swaggerJsDocs(swaggerOptions);
