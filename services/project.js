/*
 * @file: project.js
 * @description: It Contain function layer for project service.
 * @author: Jasdeep Singh
 */

import Project from "../collections/project";
import { LIMIT } from "../utilities/constants";

/************ Add Project **********/
export const addProject = async payload => {
  return await Project.add({
    ...payload
  });
};
/************ Get Project list **********/
export const projectList = async payload => {
  const search = payload["search"] ? payload["search"] : "";
  const regex = new RegExp(`${search}`, "i");
  const projects = Project.findByCondition({
    $or: [{ name: { $regex: regex } }]
  });
  const skip = payload["pageNumber"]
    ? (Number(payload["pageNumber"]) - 1) * LIMIT.PROJECTS
    : 0;
  return {
    records: await projects
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(LIMIT.PROJECTS)
      .select({ __v: 0, updatedAt: 0, loginToken: 0, password: 0 }),
    total: await projects.count(),
    limit: LIMIT.PROJECTS
  };
};
/************ Update Project **********/
export const updateProject = async payload => {
  return await Project.updateProject(payload);
};
