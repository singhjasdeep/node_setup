/*
 * @file: constants.js
 * @description: It Contain all constants for application.
 * @author: Jasdeep Singh
 */

/****************** Constants ******************/

export const ROLE = {
  VP: 1,
  ADMIN: 2,
  MANAGER: 3,
  ASSOCIATE: 4,
  HR: 5
};

export const LIMIT = {
  USERS: 10,
  PROJECTS: 10
};

export const DEPARTMENT = {
  OS: "OS",
  BD: "BD",
  MOBILITY: "MOBILITY",
  BDG: "BDG",
  MOD: "MOD"
};

export const PAYMENT_MODE = {
  FIXED: "fixed",
  HOURLY: "hourly"
};
